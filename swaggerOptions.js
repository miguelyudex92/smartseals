const options = {
    definition: {
        openapi: "3.0.0",
        info: {
           title: "SmartSeals",
           version: "1.0.0",
           description: "SmartSeals"
        },
        servers: [
            {
                url: `http://localhost:5001`,
                description: "Development Server"
            }
        ]
    },
    apis: ["./dist/routes/*.js", "./dist/index.js", "./build/routes/*.js", "./build/index.js"]
}

module.exports = options