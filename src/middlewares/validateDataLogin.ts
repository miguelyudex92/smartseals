import { typesUsers } from '../interfaces/typesUsers';

//Valida información del Login de usuario
const validateDataLogin = (req: any, res: any, next: any) => {

    let loginRequest: typesUsers = req.body
    
    if(loginRequest.hasOwnProperty("username") && loginRequest.hasOwnProperty("password")){
        if (loginRequest.username.length === 0 || loginRequest.password.length === 0) {
 
            return res.status(400).json(
                {
                    message: 'Los campos username y password no pueden estar vacíos.',
                    error: true
                }
            );
    
        } else {
            next();
        }
    }else{
        return res.status(400).json(
            {
                message: 'Debe ingresar usuario y contraseña para poder hacer login en nuestra app.',
                error: true
            }
        );
    }
    
    
};

module.exports = validateDataLogin