import { typesVehicles } from '../interfaces/typesVehicles';

//Valida información obtenida del request
const validateDataVehicle = (req: any, res: any, next: any)  => {

    let vehicleRequest: typesVehicles = req.body

    if (vehicleRequest.hasOwnProperty("brand") && vehicleRequest.hasOwnProperty("vehicle_type")) {
        if (vehicleRequest.brand.length === 0 || vehicleRequest.vehicle_type.length === 0) {

            return res.status(400).json(
                {
                    message: 'Los campos brand y vehicle_type no pueden estar vacíos.',
                    error: true
                }
            );

        } else {
            next();
        }
    } else {
        return res.status(400).json(
            {
                message: 'Los campos son obligatorios.',
                error: true
            }
        );
    }


};

module.exports = { validateDataVehicle }