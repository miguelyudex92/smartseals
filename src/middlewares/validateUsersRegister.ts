import { typesUsers } from '../interfaces/typesUsers';

//Valida información del registro de usuario
const validateUserRegisterData = (req: any, res: any, next: any) => {

    let userRequest: typesUsers = req.body

    if(userRequest.hasOwnProperty("username") && userRequest.hasOwnProperty("email") && userRequest.hasOwnProperty("password")){
        if (userRequest.username.length === 0 || userRequest.email.length === 0 || userRequest.password.length === 0) {
 
            return res.status(400).json(
                {
                    message: 'Debe ingresar la información completa, los campos no pueden estar vacíos.',
                    error: true
                }
            );
    
        } else {
            next();
        }
    }else{
        return res.status(400).json(
            {
                message: 'Debe ingresar la información completa para poder registrarse en el sistema.',
                error: true
            }
        );
    }
    
    
};

module.exports = validateUserRegisterData