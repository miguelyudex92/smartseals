import request from 'supertest';

const requesttesting = request('http://localhost:5001');

describe('POST - /service/v1/vehicles/create', () => {
    test('Probando el método Registro de vehiculo', async () => {
        let token = process.env.TOKEN_TESTING
        const response = await requesttesting
            .post('/service/v1/vehicles/create')
            .set('Authorization', `Bearer ${token}`)
            .send(
                {
                    brand: "toyota",
                    vehicle_type: "campero test"
                }
            )
        expect(response.status).toBe(200);
        console.log(response.status);
    }, 500000)

})