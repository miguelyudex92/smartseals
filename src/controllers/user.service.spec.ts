import request from 'supertest';

const requesttesting = request('http://localhost:5001');

describe('POST - /users/register', () => {
    test('Probando el método registerUser', async () => {
        const response = await requesttesting
            .post('/users/register')
            .send({
                username: "miguelgarcia_test",
                password: "admin1_test",
                email: "miguel.yudextesting@gmail.com"
            })
        expect(response.status).toBe(200);
        console.log(response.status);
    }, 500000)

})