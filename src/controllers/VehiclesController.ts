import { DaoVehicles } from '../dao/DaoVehicles';
import { VerifyToken } from '../middlewares/verifyToken';
import { Response } from 'express';
import { typesVehicles } from '../interfaces/typesVehicles';
const daoVehicles = new DaoVehicles();
const verifyToken: VerifyToken = new VerifyToken();


export class VehiclesController {

    /**
     * Controla la petición para crear vehiculo
     */
    public async createVehicle(req: any, res: Response) {

        try {
            const token = req.token;
            let requestVehicle: typesVehicles = req.body;
            try {
                let { username } = await verifyToken.verify(token);
                requestVehicle.owner_vehicle = username

            } catch (error) {
                return res.status(403).json(
                    {
                        message: "No esta autorizado para realizar esta operación",
                        error: true
                    }
                )
            }


            await daoVehicles.insertvehicle(requestVehicle)

            return res.status(200).json(
                {
                    message: `Vehiculo creado correctamente`,
                    error: false
                }
            )

        } catch (error) {
            console.log(error)
            return res.status(500).json(
                {
                    message: "Ha ocurrido un error interno",
                    error: true
                }
            )
        }

    }

    /**
     * Controla la petición para listar todos los vehiculos
     */
    public async getAllVehicles(req: any, res: Response) {


        const { limit }: any = req.query;
        const token = req.token;

        try {
            await verifyToken.verify(token);
            let data: any[] = await daoVehicles.getAllVehicles(limit);

            return res.status(200).json(
                {
                    message: data,
                    error: false
                }
            )
        } catch (error) {
            return res.status(403).json(
                {
                    message: "No está autorizado para realizar la operación",
                    error: true
                }
            )
        }


    }

    /**
     * Controla la petición para obtener vehiculo por ID
     */
    public async getVehicleById(req: any, res: Response) {

        const { id }: any = req.params;
        const token = req.token;

        try {
            await verifyToken.verify(token);
            let data: any[] = await daoVehicles.getVehicleById(typeof (id) != 'number' ? parseInt(id) : id);

            return res.status(200).json(
                {
                    message: data,
                    error: false
                }
            )
        } catch (error) {
            return res.status(403).json(
                {
                    message: "No está autorizado para realizar la operación",
                    error: true
                }
            )
        }


    }

    /**
    * Controla la petición para actualizar vehiculo
    */
    public async updateVehicle(req: any, res: Response) {

        try {
            const { id }: any = req.params;
            const token = req.token;
            let requestVehicle: typesVehicles = req.body;
            try {
                let { username } = await verifyToken.verify(token);
                requestVehicle.owner_vehicle = username

            } catch (error) {
                return res.status(403).json(
                    {
                        message: "No esta autorizado para realizar esta operación",
                        error: true
                    }
                )
            }

            await daoVehicles.updateVehicle(requestVehicle, typeof (id) != 'number' ? parseInt(id) : id);

            return res.status(200).json(
                {
                    message: `Vehiculo con id ${id} actualizado correctamente`,
                    error: false
                }
            )

        } catch (error) {
            console.log(error)
            return res.status(500).json(
                {
                    message: "Ha ocurrido un error interno",
                    error: true
                }
            )
        }

    }

    /**
    * Controla la petición para eliminar vehiculo
    */
    public async deleteVehicle(req: any, res: Response) {

        try {
            const { id }: any = req.params;
            const token = req.token;
            try {
                await verifyToken.verify(token);


            } catch (error) {
                return res.status(403).json(
                    {
                        message: "No esta autorizado para realizar esta operación",
                        error: true
                    }
                )
            }

            await daoVehicles.deleteVehicle(typeof (id) != 'number' ? parseInt(id) : id);

            return res.status(200).json(
                {
                    message: `Vehiculo con id ${id} eliminado correctamente`,
                    error: false
                }
            )

        } catch (error) {
            console.log(error)
            return res.status(500).json(
                {
                    message: "Ha ocurrido un error interno",
                    error: true
                }
            )
        }

    }

}
