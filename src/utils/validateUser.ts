import {DaoUsers} from '../dao/DaoUsers';
const users: DaoUsers = new DaoUsers();


//Valida si el usuario existe en la base de datos cuando se hace login
const validateUser = async (username: string) => {
    return new Promise<void>(async (resolve, reject) => {

        try {
            const resultado = await users.getUserByUsername(username)

            if(resultado.length > 0) {
                resolve(resultado);
            } else {
                reject("Lo sentimos no existe el usuario.")
            }
        } catch (error) {
            reject(error)
        }
    })

}

//Valida si el usuario existe en la base de datos cuando se registra
const existUsername = async (username: string) : Promise<boolean> => {
    return new Promise<boolean>(async (resolve, reject) => {

        try {
            const resultado = await users.getUserByUsername(username)

            if(resultado.length > 0) {
                resolve(true);
            } else {
                resolve(false)
            }
        } catch (error) {
            reject(error)
        }
    })

}

module.exports = {validateUser, existUsername};