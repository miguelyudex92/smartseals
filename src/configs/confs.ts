export interface configsTypes {
    PORT?:string,
    secretKey?: string,
    userMongoDb?:string,
    passwordMongoDb?:string,
    uriMongoDb?: string,
    hostMysql?:string,
    portMysql?:string,
    userMysql?:string,
    passwordMysql?:string,
    databaseMysql?:string

    
}


const confs : configsTypes  = {
    
    PORT: process.env.PORT,
    secretKey: process.env.SECRETKEY,
    userMongoDb: process.env.USERMONGODB,
    passwordMongoDb: process.env.PASSWORDMONGODB,
    uriMongoDb : process.env.URIMONGODB,
    hostMysql : process.env.HOST_MYSQL,
    portMysql: process.env.PORT_MYSQL,
    userMysql:process.env.USERMYSQL,
    passwordMysql:process.env.PASSWORDMYSQL,
    databaseMysql:process.env.DATABASE

}

module.exports = confs