import express from 'express';
const router = express.Router();
const {registerUser} = require('../controllers/UsersController')
const validateUserRegisterData = require('../middlewares/validateUsersRegister');

/**
 * @swagger
 * components:
 *  schemas:
 *      User:
 *          type: object
 *          properties:
 *              username:
 *                  type: string
 *                  description: Nombre del usuario
 *              email:
 *                  type: string
 *                  description: correo del usuario
 *              password:
 *                  type: string
 *                  description: contraseña
 *          required:
 *              - username
 *              - email
 *              - password
 *          example:
 *              username: Miguel.garcia
 *              email: miguel.garcia@gmail.com
 *              password: admin1
 */

/**
 * @swagger 
 * /users/register:
 *  post:
 *      summary: Registro de usuario en la aplicación
 *      parameters:
 *                - in: body
 *                  name: body    
 *                  description: Body
 *                  schema:
 *                      $ref: '#/components/schemas/User'
 *      responses: 
 *          200:
 *             description: User created!
 *             
 *             
 */

router.post('/register',validateUserRegisterData, registerUser);

module.exports = router;