import {Router} from 'express';
const AuthLogin = require('../controllers/AuthLoginController');
const validateDataLogin = require('../middlewares/validateDataLogin');
const router = Router();


/**
 * @swagger
 * components:
 *  schemas:
 *      Login:
 *          type: object
 *          properties:
 *              username:
 *                  type: string
 *                  description: Nombre del usuario
 *              password:
 *                  type: string
 *                  description: contraseña
 *          required:
 *              - username
 *              - password
 *          example:
 *              username: Miguel.garcia
 *              password: admin1
 */


/**
 * @swagger 
 * /service/v1/authorization/login:
 *  post:
 *      summary: Inicia sesión con un nombre de usuario y password, el servidor devolvera un token si el usuario existe en la base de datos
 *      parameters:
 *                - in: body
 *                  name: body    
 *                  description: Body
 *                  schema:
 *                      $ref: '#/components/schemas/Login'
 *      responses: 
 *          200:
 *             description: Autenticación correcta
 *             content: 
 *             application/json:
 */
router.post('/login',validateDataLogin, AuthLogin);

module.exports = router;