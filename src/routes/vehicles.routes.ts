import express, { Router } from "express";
import { VehiclesController } from '../controllers/VehiclesController';
const { validateDataVehicle } = require('../middlewares/validateDataVehicle');


const router: Router = express.Router();
const vehiclesController: VehiclesController = new VehiclesController();


/**
 * @swagger
 * components:
 *  schemas:
 *      Vehicle:
 *          type: object
 *          properties:
 *              brand:
 *                  type: string
 *                  description: marca del vehiculo
 *              vehicle_type:
 *                  type: string
 *                  description: tipo de vehículo
 *          required:
 *              - brand
 *              - vehicle_type
 *          example:
 *              brand: chevrolet
 *              vehicle_type: campero
 *              
 */


/**
 * @swagger 
 * /service/v1/vehicles/getAll?limit=${limit}:
 *  get:
 *      summary: Obtiene listado de vehículos y opcionalmente recibe el limite de resultados a recibir
 *      responses: 
 *          200:
 *             description: Lista de vehiculos
 *             content:
 *                application/json:
 *                  schema:
 *                    type: object
 *                    properties:
 *                      message:
 *                        type: array
 *                        items:
 *                          type: object
 *                          properties:
 *                            vehicle_id:
 *                                      type: integer
 *                                      description: Id de vehiculo
 *                                      example: 1
 *                            brand:
 *                                 type: string
 *                                 description: marca del vehiculo
 *                                 example: chevrolet
 *                            vehicle_type:
 *                                        type: string
 *                                        description: tipo de vehiculo
 *                                        example: campero
 *                            owner_vehicle:
 *                                        type: string
 *                                        description: dueño del vehiculo
 *                                        example: miguelgarcia
 *                            creation_date:
 *                                        type: date
 *                                        description: fecha de creación
 *                                        example: 2022-03-06T05:00:00.000Z
 */
router.get("/getAll", vehiclesController.getAllVehicles);

/**
 * @swagger 
 * /service/v1/vehicles/get/:id:
 *  get:
 *      summary: Obtiene un vehículo por id
 *      responses: 
 *          200:
 *             description: Vehículo
 *             content:
 *                application/json:
 *                  schema:
 *                    type: object
 *                    properties:
 *                      message:
 *                        type: array
 *                        items:
 *                          type: object
 *                          properties:
 *                            vehicle_id:
 *                                      type: integer
 *                                      description: Id de vehiculo
 *                                      example: 1
 *                            brand:
 *                                 type: string
 *                                 description: marca del vehiculo
 *                                 example: chevrolet
 *                            vehicle_type:
 *                                        type: string
 *                                        description: tipo de vehiculo
 *                                        example: campero
 *                            owner_vehicle:
 *                                        type: string
 *                                        description: dueño del vehiculo
 *                                        example: miguelgarcia
 *                            creation_date:
 *                                        type: date
 *                                        description: fecha de creación
 *                                        example: 2022-03-06T05:00:00.000Z
 */
router.get("/get/:id", vehiclesController.getVehicleById);

/**
 * @swagger 
 * /service/v1/vehicles/create:
 *  post:
 *      summary: Crea un vehículo
 *      parameters:
 *                - in: body
 *                  name: body    
 *                  description: Body
 *                  schema:
 *                      $ref: '#/components/schemas/Vehicle'
 *      responses: 
 *          200:
 *             description: Vehiculo creado correctamente                     
 */
router.post("/create", validateDataVehicle, vehiclesController.createVehicle);

/**
 * @swagger 
 * /service/v1/vehicles/update/:id:
 *  put:
 *      summary: Actualiza la información de un vehículo
 *      responses: 
 *          200:
 *             description: Vehiculo actualizado correctamente                     
 */
router.put("/update/:id", validateDataVehicle, vehiclesController.updateVehicle);

/**
 * @swagger 
 * /service/v1/vehicles/delete/:id:
 *  delete:
 *      summary: Elimina la información de un vehículo
 *      responses: 
 *          200:
 *             description: Vehiculo con id ${:id} eliminado correctamente                     
 */
 router.delete("/delete/:id", vehiclesController.deleteVehicle);

module.exports = router;