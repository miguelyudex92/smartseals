import { typesVehicles } from '../interfaces/typesVehicles';
const { MySQLPool } = require('../connectors/Mysql');


export class DaoVehicles {

    private table_name:string = "vehicles";

    /**
     * Inserta registro en la tabla vehicles
     */
    public insertvehicle(data:typesVehicles) {
        return new Promise((resolve, reject) => {
            MySQLPool.getConnection((err: any, connection: any) => {

                if (err) reject(err);
                connection.query(this.queryInsertManagement(),
                    [data.brand, data.vehicle_type, data.owner_vehicle],
                    (error: any, results: any) => {
                        if (error) {
                            console.error(error);
                            let errorResponse: any = JSON.parse(JSON.stringify(error));
                            connection.release();
                            reject(errorResponse);
                        } else {
                            connection.release();
                            resolve(results);
                        }
                    }
                )
            })
        });
    }

    /**
     * devuelve una lista limitada de vehiculos
     */
     public getAllVehicles(limit:string|undefined): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            let limite = limit ?? 10
            MySQLPool.getConnection((err: any, connection: any) => {

                if (err) reject(err);
                connection.query(this.querygetAllVehicles(typeof(limite) != 'number' ? parseInt(limite) : limite),
                    [],
                    (error: any, results: any[]) => {
                        if (error) {
                            console.error(error);
                            let errorResponse: any = JSON.parse(JSON.stringify(error));
                            connection.release();
                            reject(errorResponse);
                        } else {
                            connection.release();
                            let result = JSON.parse(JSON.stringify(results));
                            resolve(result);
                        }
                    }
                )
            })
        });
    }

    /**
     * devuelve vehiculo por ID
     */
     public getVehicleById(id:number): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            
            MySQLPool.getConnection((err: any, connection: any) => {

                if (err) reject(err);
                connection.query(this.querygetVehicleById(id),
                    [],
                    (error: any, results: any[]) => {
                        if (error) {
                            console.error(error);
                            let errorResponse: any = JSON.parse(JSON.stringify(error));
                            connection.release();
                            reject(errorResponse);
                        } else {
                            connection.release();
                            let result = JSON.parse(JSON.stringify(results));
                            resolve(result);
                        }
                    }
                )
            })
        });
    }

    /**
     * Actualiza registro en la tabla vehicles
     */
     public updateVehicle(data:typesVehicles, id:number) {
        return new Promise((resolve, reject) => {
            MySQLPool.getConnection((err: any, connection: any) => {

                if (err) reject(err);
                connection.query(this.queryUpdateVehicle(id),
                    [data.brand, data.vehicle_type, data.owner_vehicle],
                    (error: any, results: any) => {
                        if (error) {
                            console.error(error);
                            let errorResponse: any = JSON.parse(JSON.stringify(error));
                            connection.release();
                            reject(errorResponse);
                        } else {
                            connection.release();
                            resolve(results);
                        }
                    }
                )
            })
        });
    }

    /**
     * Elimina registro en la tabla vehicles
     */
     public deleteVehicle(id:number) {
        return new Promise((resolve, reject) => {
            MySQLPool.getConnection((err: any, connection: any) => {

                if (err) reject(err);
                connection.query(this.queryDeleteVehicle(id),
                    [],
                    (error: any, results: any) => {
                        if (error) {
                            console.error(error);
                            let errorResponse: any = JSON.parse(JSON.stringify(error));
                            connection.release();
                            reject(errorResponse);
                        } else {
                            connection.release();
                            resolve(results);
                        }
                    }
                )
            })
        });
    }



    //Queries

    private queryInsertManagement(): string {
        let sqlInsert = `INSERT INTO ${this.table_name}
        (brand, vehicle_type, owner_vehicle, creation_date)
        VALUES(
            ?,
            ?,
            ?,
            NOW()
        )`;
        return sqlInsert;
    }

    private querygetAllVehicles(limit:number): string {
        let sqlInsert = `SELECT * FROM ${this.table_name} limit ${limit};`;
        return sqlInsert;
    }

    private querygetVehicleById(id:number): string {
        let sqlInsert = `SELECT * FROM ${this.table_name} WHERE vehicle_id=${id};`;
        return sqlInsert;
    }

    private queryUpdateVehicle(id:number): string {
        let sqlInsert = `UPDATE ${this.table_name}
        SET brand=?,
        vehicle_type=?,
        owner_vehicle=?
        WHERE vehicle_id=${id};`;
        return sqlInsert;
    }

    private queryDeleteVehicle(id:number): string {
        let sqlInsert = `DELETE FROM ${this.table_name}
        WHERE vehicle_id=${id};`;
        return sqlInsert;
    }

}