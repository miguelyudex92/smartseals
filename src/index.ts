import express, { Request, Response } from 'express';
import bodyParser from "body-parser";
import { configsTypes } from './configs/confs';

const cors = require('cors');
const app = express();
require('dotenv').config();
const confs: configsTypes = require('./configs/confs');
const vehiclesRoutes = require('./routes/vehicles.routes');
const userRoutes = require('./routes/users.routes');
const validateToken = require('./middlewares/validateToken');
const routerAuth = require('./routes/auth.routes');
app.use(bodyParser.json());
app.use(cors());


//Swagger
const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');
const options = require('../swaggerOptions.js');

//Configurar swagger jsdoc
const specs = swaggerJsDoc(options)

//Up Server
app.listen(confs.PORT, () => {
    console.log(`Server on port ${confs.PORT}`);
});

//Require Connection Database
require('./connectors/Mongo');

app.get('/', (req: Request, res: Response) => {
    res.status(200).send(`API Smart Seals - Service Up
        ${new Date().toLocaleString()} (PORT: ${process.env.PORT})`);
});



//Register User
app.use("/users", userRoutes);

//Routes Authorization
app.use('/authorization', routerAuth);

//Api Vehicles
app.use("/service/v1/vehicles", validateToken, vehiclesRoutes);

//Routes Swagger API
app.use('/service/v1/docs', swaggerUI.serve, swaggerUI.setup(specs));

module.exports = app;