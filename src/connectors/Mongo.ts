const mongoose  = require('mongoose');
import { configsTypes } from '../configs/confs';


const confs: configsTypes = require('../configs/confs');

const connectionParams = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}

mongoose.connect(confs.uriMongoDb, connectionParams)
.then((db:any) => {
    console.log(`Db Is connected to ${db.connection.host}`);
})
.catch((err:any) => console.error(`[ERROR] - Ha ocurrido un error al intentar abrir la conexión: ${err}`));