import { Connection } from "mysql";
import { configsTypes } from '../configs/confs';
const mysql = require('mysql');
const confs: configsTypes = require('../configs/confs');

const MySQLPool = mysql.createPool({
    host: confs.hostMysql,
    port: confs.portMysql,
    user: confs.userMysql,
    password: confs.passwordMysql,
    database: confs.databaseMysql,
    multipleStatements: true
});

MySQLPool.getConnection(function(err:any, connection:Connection) {
    if(err) {
        console.error('No se pudo conectar a la base de datos MySql: ' + err)
    }
    else {
        console.info('Conectado a la base de datos MySql')
    }
    
})


module.exports = {
    MySQLPool
};