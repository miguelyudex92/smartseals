export interface typesVehicles {
    brand: string,
    vehicle_type: string,
    owner_vehicle: string
}